function renderProductList(productArr) {
  var contenHTML = "";
  productArr.forEach(function phoneItem(phone, i) {
    var content = `
    <tr>
      <td>${i + 1}</td>
      <td>${phone.name}</td>
      <td>$${phone.price}</td>
      <td><img style="width:80px;" src="${phone.img}" alt="${phone.name}"></td>
      <td>${phone.backCamera}</td>
      <td>${phone.frontCamera} </td>
      <td>${phone.screen}</td>
      <td>${phone.desc}  <br /><br />
        <span>Lọai thiết bị:
          <span class="font-weight-bold">
            ${phone.type}
          </span>
        </span>
      </td>
      <td style="text-align: center;width: 92px;">
        <button onclick="xoaPhone('${phone.id}')" class="btn btn-danger m-1">
          <i class="fa-solid fa-trash-can"></i>
        </button>
        <button onclick="suaPhone('${
          phone.id
        }')" data-toggle="modal" data-target="#exampleModal" class="btn btn-success m-1">
          <i class="fa-solid fa-rotate"></i>
        </button>
      </td>
    </tr>`;
    contenHTML += content;
  });
  document.getElementById("tbodyDataPhone").innerHTML = contenHTML;
}

// LÁY THÔNG TIN ĐỂ SỬA 
function layThongTinTuForm() {
  var name = document.getElementById("tenPhone").value;
  var price = document.getElementById("gia").value;
  var screen = document.getElementById("manHinh").value;
  var backCamera = document.getElementById("camSau").value;
  var frontCamera = document.getElementById("camTruoc").value;
  var img = document.getElementById("anhLink").value;
  var desc = document.getElementById("moTa").value;
  var type = document.getElementById("loaiPhone").value;
  if (
    !name ||
    !price ||
    !screen ||
    !backCamera ||
    !frontCamera ||
    !img ||
    !desc ||
    !type
  ) {
    alert("Vui lòng nhập đầy đủ thông tin!");
    return null;
  }
  return {
    name: name,
    price: price,
    screen: screen,
    backCamera: backCamera,
    frontCamera: frontCamera,
    type: type,
    img: img,
    desc: desc,
  };
}
// SHOW THÔNG TIN LÊN FORM
function showThongTinLenForm(phone) {
  document.getElementById("tenPhone").value = phone.name;
  document.getElementById("gia").value = phone.price;
  document.getElementById("manHinh").value = phone.screen;
  document.getElementById("camSau").value = phone.backCamera;
  document.getElementById("camTruoc").value = phone.frontCamera;
  document.getElementById("loaiPhone").value = phone.type;
  document.getElementById("anhLink").value = phone.img;
  document.getElementById("moTa").value = phone.desc;
}

// RESET
function resetInput() {
  document.getElementById("tenPhone").value = "";
  document.getElementById("gia").value = "";
  document.getElementById("manHinh").value = "";
  document.getElementById("camSau").value = "";
  document.getElementById("camTruoc").value = "";
  document.getElementById("loaiPhone").value = "";
  document.getElementById("anhLink").value = "";
  document.getElementById("moTa").value = "";
}

// LOADING
function batLoading() {
  document.getElementById("spinner").style.display = "flex";
}
function tatLoading() {
  document.getElementById("spinner").style.display = "none";
}


// SẮP XẾP THEO GIÁ TIỀN
function giaTuThapDenCao(productArr) {
  productArr.sort(function (a, b) {
    return a.price - b.price;
  });
  renderProductList(productArr);
}

function giaTuCaoDenThap(productArr) {
  productArr.sort(function (a, b) {
    return b.price - a.price;
  });
  renderProductList(productArr);
}



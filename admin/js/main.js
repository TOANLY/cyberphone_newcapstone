var idProductUpdate = null;
var productArr = [];

function fetchProducList() {
  batLoading();
  productServ
    .getList()
    .then(function (res) {
      renderProductList(res.data);
      tatLoading();
    })
    .catch(function (err) {
      tatLoading();
    });
}
fetchProducList();

function xoaPhone(id) {
  batLoading();
  productServ
    .delete(id)
    .then(function (res) {
      fetchProducList();
      renderProductlist(res.data);
      tatLoading();
    })
    .catch(function (err) {
      tatLoading();
    });
}

function addPhone() {
  var newProduct = layThongTinTuForm();
  productServ
    .create(newProduct)
    .then((res) => {
      fetchProducList();
      alert("Đã thêm sản phẩm");
      $("#exampleModal").modal("hide");
      resetInput();
    })
    .catch((err) => { });
}

function suaPhone(id) {
  idProductUpdate = id;
  document.getElementById("upPhone").style.display = "block";
  document.getElementById("addPhone").style.display = "none";
  productServ
    .getById(id)
    .then((res) => {
      showThongTinLenForm(res.data);
      tatLoading();
    })
    .catch((err) => {
      tatLoading();
    });
}

function upPhone() {
  var product = layThongTinTuForm();
  productServ
    .update(idProductUpdate, product)
    .then((res) => {
      fetchProducList();
      alert("Sửa sản phẩm thành công");

      $("#exampleModal").modal("hide");
      resetInput();
    })
    .catch((err) => { });
}

function modalButtonHandler() {
  document.getElementById("upPhone").style.display = "none";
  document.getElementById("addPhone").style.display = "block";
  resetInput();
}
document
  .getElementById("modalButton")
  .addEventListener("click", modalButtonHandler);

function sapXepGia() {
  const sortSelect = document.getElementById("sort");
  const value = sortSelect.value;
  if (value === "low-to-high") {
    giaTuThapDenCao(productArr);
  } else if (value === "high-to-low") {
    giaTuCaoDenThap(productArr);
  }
}

document.getElementById("sort").addEventListener("change", sapXepGia);
function fetchProductList() {
  batLoading();
  productServ
    .getList()
    .then(function (res) {
      productArr = res.data;
      renderProductList(productArr);
      tatLoading();
    })
    .catch(function (err) {
      tatLoading();
    });
}

fetchProductList();

function searchPhone() {
  var searchName = document
    .getElementById("searchName")
    .value.trim()
    .toLowerCase();
  var searchResults = productArr.filter(function (product) {
    return product.name.toLowerCase().includes(searchName);
  });
  resetInput();
  renderProductList(searchResults);
}

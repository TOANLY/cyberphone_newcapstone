const BASE_URL = "https://648b1bd117f1536d65ea6142.mockapi.io/capstone";
var productServ = {
  getList: () => {
    return axios({
      url: BASE_URL,
      method: "GET",
    });
    //   .then(function (res) {
    //     console.log("res", res);
    //   })
    //   .catch(function (err) {
    //     console.log("err", err);
    //   });
  },
  getDetail: (id) => {
    return axios({
      url: BASE_URL + `/${id}`,
      method: "GET",
    });
    //   .then(function (res) {
    //     console.log("res", res);
    //   })
    //   .catch(function (err) {
    //     console.log("err", err);
    //   });
  },
};

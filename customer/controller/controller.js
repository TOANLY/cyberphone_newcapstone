function renderProductList(productArr) {
  var contentHTML = "";

  productArr.forEach(function (item) {
    var content = `<div class="card mr-20" style="width: 18rem; margin-right: 10px">
        <div class="hinhAnh"><img style="width: 100%;" src="${item.img}" alt=""></div>
        <div class="card-body">
          <h5 id="tenSP" >${item.name}</h5>
          <p id="giaSP">${item.price}</p>
          <div class="overlay">
            <p id="moTa">${item.desc}</p>
            <p id="manHinh">${item.screen}</p>
            <p id="cmrSau">${item.backCamera}</p>
            <p id="cmrTruoc">${item.frontCamera}</p>
          </div>
          <button onclick="addProduct(${item.id})" class="btn btn-warning">Thêm vào giỏ hàng</a>
        </div>
      </div>`;
    contentHTML += content;
  });
  document.getElementById("phone_List").innerHTML = contentHTML;
}

function renderCard(cart) {
  var contentHTML = "";
  let total = 0;

  cart.forEach(function (item) {
    total += item.price * item.soluong;
    var content = `<tr>
          <td>
            <img style="width:100px" src="${item.img}" alt="" />
          </td>
          <td>${item.name}</td>
          <td>${(item.price * 1).toLocaleString()}</td>
          <td>${item.soluong}</td>
          <td>${(item.soluong * item.price).toLocaleString()}</td>
          <td>
            <button onclick="deleteItemCart(${item.id})">Xoa</button>
          </td>
        </tr>`;
    contentHTML += content;
  });
  document.getElementById("cart-body").innerHTML = contentHTML;
  document.getElementById("total").innerHTML = total.toLocaleString();
}

function fetchProductList() {
  // render dssp từ admin
  productServ
    .getList()
    .then((res) => {
      renderProductList(res.data);
    })
    .catch((err) => {
      console.log("err", err);
    });
}
fetchProductList();

var cart = [];
// function themSP(x) {
//   var item = x.parentElement.children;
//   var hinh = item[0].children[0].src;
//   var name = item[1].children[0].innerText;
//   var gia = item[1].children[1].innerText;
//   addCart(name, id, gia);
// }
// function addCart(id, name, gia) {
//   var addTr = document.createElement("tr");
//   var content = `<tr">
//   <td class="text-left"><img style="width: 80px;"src="${hinh}"alt="">${name}</td>
//   `;
// }

// addTr.innerHTML = content;
// var contentHTML = document.querySelector("tbody");
// contentHTML.append(addTr);
console.log(cart);
const addProduct = (id) => {
  productServ
    .getDetail(id)
    .then((res) => {
      const product = res.data;

      const index = cart.findIndex((item) => item.id == product.id);

      if (index === -1) {
        cart.push({
          ...product,
          soluong: 1,
        });
      } else {
        cart[index].soluong = cart[index].soluong + 1;
      }
      renderCard(cart);
    })
    .catch((err) => {
      console.log(err);
    });
};
fetchProductList();

window.addProduct = addProduct;

const deleteItemCart = (id) => {
  productServ
    .getDetail(id)
    .then((res) => {
      const product = res.data;

      const index = cart.findIndex((item) => item.id == product.id);

      if (index === -1) {
        alert("Ko co san pham trong gio hang");
      } else {
        cart.splice(index, 1);
      }
      renderCard(cart);
    })
    .catch((err) => {
      console.log(err);
    });
};

window.deleteItemCart = deleteItemCart;
